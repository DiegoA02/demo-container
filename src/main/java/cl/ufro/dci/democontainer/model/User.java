package cl.ufro.dci.democontainer.model;

public class User {

    public String name;

    public String username;

    public User(String name, String username) {
        this.name = name;
        this.username = username;
    }
}
