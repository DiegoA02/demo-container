package cl.ufro.dci.democontainer.controller;


import cl.ufro.dci.democontainer.model.User;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api")
public class ServicioDeEjemplo {

    private static List<User> listaUsuarios = new ArrayList<User>();

    @GetMapping("/users")
    public List<User> getUsers() {
        listaUsuarios.add(new User("Juan", "Perez"));
        listaUsuarios.add(new User("Maria", "Lopez"));

        return listaUsuarios;
    }
}