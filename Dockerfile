FROM openjdk:8-jdk-alpine

VOLUME /tmp

EXPOSE 8080

COPY target/demo-container-0.0.1-SNAPSHOT.jar demo-container.jar

ENTRYPOINT ["java", "-jar", "demo-container.jar"]

#sudo docker build -t demo-container .
#sudo docker run -p 8080:8080 -d demo-container